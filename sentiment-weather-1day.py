# 
import sys, os, datetime, time

def main():
    day = "2013-12-05"
    # Get 
    sentiment = []
    with open("data/classified-latlng.csv", "r", encoding="utf-8") as f:
        for line in f:
            try:
                parts = line.strip().split("\t")
                label = parts[3]
                if parts[0][0:10] == day:
                    sentiment.append((day, parts[1], label))
            except Exception as e:
                print(e)
    print(sentiment)
    weather = {}
    var = ['FG','TG','TN','TX','SQ','DR','RH']
    with open("data/KNMI_20131231_v1.txt", "r") as f:
        for line in f:
            line = line.strip()
            if len(line) == 0 or line[0] == "#":
                continue
            parts = line.split(",")
            station = parts[0]
            date = time.strptime(parts[1], '%Y%m%d')
            d1 = datetime.date(date[0], date[1], date[2])
            weekNumber = d1.strftime("%Y-%m-%d")
            if station not in weather and weekNumber == day:
                weather[station] = {'FG':0,'TG':0,'TN':0,'TX':0,'SQ':0,'DR':0,'RH':0}
                for i in range(len(var)):
                    value = parts[i+2].strip()
                    try:
                        value = int(value)
                    except ValueError:
                        value = None
                    if value is not None:
                        if value < 0:
                            value = 0
                        weather[station][var[i]] = value
    
    # Print final data
    with open("data/tweets-"+day+".csv", "w", encoding="utf-8") as f:
        #f.write("date\tcoordinates\tlabel\n")
        for date, coor, label in sentiment:
            f.write("{}\t{}\t{}\n".format(date, coor, label))
            
    with open("data/weather-"+day+".csv", "w", encoding="utf-8") as f:
        #f.write("station\t")
        #f.write("\t".join(var))
        #f.write("\n")
        for station in weather:
            f.write("{}\t".format(station))
            for column in var:
                f.write("{}\t".format(weather[station][column]))
            f.write("\n")
    print("Done")
if __name__ == "__main__":
    main()
