# Self bootstrap script to automatically annotate more tweets.
import sys, io, heapq
import nltk.classify.util
from nltk.classify import NaiveBayesClassifier

def getNgramN():
    return 1

def getLabels():
    return ["p", "n", "o"]

def main():
    labels = getLabels()
    batchSize = 100
    labeled = loadLabeledSet("data/train_annotated1005.csv")
    testLabeled = loadLabeledSet("data/test_annotated1005.csv")
    unlabeled = loadSet("data/train_bootstrap.csv")
    unlabeled = unlabeled[:500]

    
    print("{} tweets manually annotated.".format(len(labeled)))
    print("{} tweets ready to be self bootstrapped.".format(len(unlabeled)))
    startIndex = 0
    bootstrapped = 0
    print("bootstrapped\taccuracy")
    while startIndex < len(unlabeled):
        print("Training classifier with {} tweets. ".format(len(labeled)))
        classifier = NaiveBayesClassifier.train(labeled)
        classified = []

        for tweet in unlabeled[startIndex:]:
            featureDict = getFeatureDict(tweet)
            label = classifier.classify(featureDict)
            chances = classifier.prob_classify(featureDict)
            # (featureDict, label, confidence)
            classified.append((featureDict, label, chances.prob(label)))

        # Sort by confidence, limit to the batchSize and add to labeled list
        classified = heapq.nlargest(min(batchSize, len(classified)), classified, key=lambda tup: tup[2])
        bootstrapped += len(classified)
        for i in range(len(classified)):
            labeled.append((classified[i][0], classified[i][1]))
        print("Added best {} tweets to labeled set...".format(len(classified)))
        startIndex += batchSize
    print("Done with bootstrapping.")
    
    print("Training for the last time...")
    classifier = NaiveBayesClassifier.train(labeled)
    
    print("Done training, testing...")
    confusion, correctCount, totalCount = testClassifier(classifier, testLabeled)
    print("Ngram n = {}".format(getNgramN()))
    print("Batchsize = {}".format(batchSize))
    print("Confusion Matrix")
    print("ex\\ac\tp\tn\to")
    for ex in getLabels():
        print("{}\t{}\t{}\t{}".format(ex, confusion[ex]["p"], confusion[ex]["n"], confusion[ex]["o"]))

    
    print("")
    print("Correct: {}/{} = {}".format(correctCount, totalCount, correctCount/totalCount))
    classifier.show_most_informative_features()
    classifyAndExport(classifier, loadSet("data/train_bootstrap.csv"), "data/classified.csv")
    print("Done!")


def testClassifier(classifier, testLabeled):
    correctCount = 0
    # {expectedValue:{actualValue:n, ...}, ...}
    confusion = {"p":{"p":0, "n":0, "o":0}, "n":{"p":0, "n":0, "o":0}, "o":{"p":0, "n":0, "o":0}}
    for tweet, expected in testLabeled:
        predicted = classifier.classify(tweet)
        confusion[expected][predicted] += 1
        if predicted == expected:
            correctCount += 1
    return (confusion, correctCount, len(testLabeled))

def classifyAndExport(classifier, data, outFile):
    with open(outFile, "w", encoding="utf-8") as out:
        for tweet in data:
            featureDict = getFeatureDict(tweet)
            label = classifier.classify(featureDict)
            out.write("{}\t{}\n".format(tweet, label))

def getFeatureDict(tweet):
    return dict([(word, True) for word in getTokens(tweet)])

def getTokens(body):
    return ngrams(body.split(" "), getNgramN())

def ngrams(tokens, n):
    if len(tokens) < n:
        return [" ".join(tokens)]
    output = []
    for i in range(len(tokens)-n+1):
        output.append(" ".join(tokens[i:i+n]))
    return output

def loadLabeledSet(file):
    data = []
    i = 0
    with open(file, "r", encoding="utf-8") as stream:
        for line in stream:
            try:
                tweet = parseTweet(line)
                if isValidTweet(tweet, True):
                    data.append((getFeatureDict(tweet[8]), tweet[7]))
            except:
                print("Error on line {} in file {}".format(i, file))
            i += 1
    return data

def loadSet(file):
    data = []
    with open(file, "r", encoding="utf-8") as stream:
        for line in stream:
            tweet = parseTweet(line)
            if isValidTweet(tweet, False):
                data.append(tweet[8])
    return data

# Tested all cases via console
def isValidTweet(tweet, onlyAnnotated):
    # Tweet with label?
    if onlyAnnotated and tweet[7] not in getLabels():
        return False
    # Prevent vague words for unannotated tweets
    if (tweet[7] == "" or tweet[7] == "a") and (" mist " in tweet[8] or " het weer " in tweet[8]):
        return False
    return True

# Returns the split and cleaned tweet based on the given line
def parseTweet(tweet):
    parts = tweet.strip().split(";", 8)
    if parts[8][0] == "\"":
        parts[8] = parts[8][1:]
    if parts[8][-1] == "\"":
        parts[8] = parts[8][:-1]
    return parts

if __name__ == "__main__":
    main()
