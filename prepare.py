# Used to convert the tweets to a csv format that is usable in Excel.
import sys, os

def main():
    with open("data/filtered.csv", "r", encoding="utf-8") as streamIn, open("data/filtered_prepared.csv", "w", encoding="utf-8") as streamOut:
        for tweet in streamIn:
            # part 0 is the coordinates; 1 is the location description; 7 was the body, but becomes the class; 8 becomes the body
            parts = tweet.split("\t")
            parts[1] = "\""+parts[1]+"\""
            parts[3] = "\""+parts[3]+"\""
            parts.append("\""+parts[7][:-1]+"\"")
            parts[7] = ""
            streamOut.write(";".join(parts)+"\n")
if __name__ == "__main__":
    main()