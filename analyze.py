# Script for analyzing automatically classified tweets.
# for example: counting for certain words how often they are used in positive/negative/objective tweets
import sys, io, heapq
import nltk.classify.util
from nltk.classify import NaiveBayesClassifier


def main():
    ## Constansts
    inFile = "data/classified.csv"
    tracking = ["❤", "zonnig", "regen", "sneeuw", "hagel", "regenboog", "blauwe lucht", "wind", "storm", "warm", "koud"]
    labels = ["p", "n", "o"]

    ## Setup Counter
    counter = {}
    for track in tracking:
        counter[track] = {"p":0, "n":0, "o":0}

    ## Count
    with open(inFile, "r", encoding="utf-8") as f:
        for line in f:
            date, tweet, label = line.strip().split("\t")
            for track in tracking:
                if track in tweet:
                    counter[track][label] += 1
    ## Display Results
    print("{}\t{}\t{}\t{}\t{}".format("Word".ljust(20), "total", "p", "n", "o"))
    for track in tracking:
        total = counter[track]["p"] + counter[track]["n"] + counter[track]["o"]
        print("{}\t{}\t{:.2f}\t{:.2f}\t{:.2f}".format(track.ljust(20), total, counter[track]["p"]/total, counter[track]["n"]/total, counter[track]["o"]/total))
    

if __name__ == "__main__":
    main()
