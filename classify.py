# Script to test the main classifier and to classify 80000 tweets for other experiments
import sys, io, heapq, csv
import nltk.classify.util
from nltk.classify import NaiveBayesClassifier
from nltk.tokenize import word_tokenize
from urllib.parse import urlsplit

def getNgramN():
    return 1

def getLabels():
    return ["p", "n", "o"]

def main():
    labels = getLabels()
    labeled = loadLabeledSet("data/train_annotated1005.csv")
    testLabeled = loadLabeledSet("data/test_annotated1005.csv")
    print("{} tweets manually annotated for training.".format(len(labeled)), end=" ")
    countLabels(labeled)
    print("{} tweets manually annotated for testing.".format(len(testLabeled)), end=" ")
    countLabels(testLabeled)
    classifier = NaiveBayesClassifier.train(labeled)
    
    print("Done training, testing...")
    confusion, correctCount, totalCount = testClassifier(classifier, testLabeled)
    print("Ngram n = {}".format(getNgramN()))
    print("Confusion Matrix")
    print("ex\\ac\tp\tn\to")
    for ex in getLabels():
        print("{}\t{}\t{}\t{}".format(ex, confusion[ex]["p"], confusion[ex]["n"], confusion[ex]["o"]))

    
    print("")
    print("Correct: {}/{} = {}".format(correctCount, totalCount, correctCount/totalCount))
    classifier.show_most_informative_features(20)
    classifyAndExport(classifier, loadSet("data/train_bootstrap.csv"), "data/classified-latlng.csv")
    print("Done!")

def countLabels(labeledSet):
    counter = {"p":0, "n":0, "o":0}
    for item in labeledSet:
        counter[item[1]] += 1
    print(counter)

def testClassifier(classifier, testLabeled):
    correctCount = 0
    # {expectedValue:{actualValue:n, ...}, ...}
    confusion = {"p":{"p":0, "n":0, "o":0}, "n":{"p":0, "n":0, "o":0}, "o":{"p":0, "n":0, "o":0}}
    for tweet, expected in testLabeled:
        predicted = classifier.classify(tweet)
        confusion[expected][predicted] += 1
        if predicted == expected:
            correctCount += 1
    return (confusion, correctCount, len(testLabeled))

def classifyAndExport(classifier, data, outFile):
    with open(outFile, "w", encoding="utf-8") as out:
        for date, coor, tweet in data:
            featureDict = getFeatureDict(tweet)
            label = classifier.classify(featureDict)
            out.write("{}\t{}\t{}\t{}\n".format(date, coor, tweet.strip(), label))

def getFeatureDict(tweet):
    return dict([(word, True) for word in getTokens(tweet)])

def getTokens(body):
    return ngrams(normalizeTokens(word_tokenize(body, language='dutch')), getNgramN())

def normalizeTokens(tokens):
    remove = []
    for i in range(len(tokens)):
        token = tokens[i].lower()
        if tokens[i] == "http" and tokens[i + 1] == ":":
            link = "http:"+tokens[i + 2]
            tokens[i] = urlsplit(link).netloc
            remove.append(i + 1)
            remove.append(i + 2)
    for index in reversed(remove):
        del tokens[index]
    return tokens

def ngrams(tokens, n):
    if len(tokens) < n:
        return [" ".join(tokens)]
    output = []
    for i in range(len(tokens)-n+1):
        output.append(" ".join(tokens[i:i+n]))
    return output

def loadLabeledSet(file):
    data = []
    i = 0
    with open(file, "r", encoding="utf-8") as f:
        reader = csv.reader(f, delimiter=";")
        for line in reader:
            try:
                if isValidTweet(line, True):
                    data.append((getFeatureDict(line[8]), line[7]))
            except:
                print("Error on line {} in file {}".format(i, file))
            i += 1
    return data

def loadSet(file):
    data = []
    with open(file, "r", encoding="utf-8") as f:
        reader = csv.reader(f, delimiter=";")
        for line in reader:
            if isValidTweet(line, False):
                data.append((line[4], line[0], line[8]))
    return data

# Tested all cases via console
def isValidTweet(tweet, onlyAnnotated):
    # Tweet with label?
    if onlyAnnotated and tweet[7] not in getLabels():
        return False
    # Prevent vague words for unannotated tweets
    if (tweet[7] == "" or tweet[7] == "a") and (" mist " in tweet[8] or " het weer " in tweet[8]):
        return False
    return True


if __name__ == "__main__":
    main()
