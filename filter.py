# Used to filter the main dataset (only tweets about the weather and from the Netherlands).
import sys, os

def main():
    weatherWordsFile = "weatherwords.txt"
    inputFile = "/net/shared/CorpusLinguistics/Geolocated/geolocated-tweets-2012-2014.csv"
    outputFile = "filtered.csv"

    weatherWords = []
    with open(weatherWordsFile, "r", encoding="utf-8") as words:
        for word in words:
            word = word.strip()
            if len(word) > 0:
                weatherWords.append(word)
                weatherWords.append("#"+word)

    with open(inputFile, "r", encoding="utf-8") as streamIn, open(outputFile, "w", encoding="utf-8") as streamOut:
        for tweet in streamIn:
            # part 0 is the coordinates, 1 is the location description, 7 is the body
            parts = tweet.split("\t")
            if parts[1].endswith("The Netherlands") and not (parts[7].startswith("RT @")):
                words = parts[7].split(" ")
                for i in range(len(words)):
                    if words[i] in weatherWords or (i+1 < len(words) and words[i]+" "+words[i+1] in weatherWords):
                        streamOut.write(tweet)
                        break

if __name__ == "__main__":
    main()
