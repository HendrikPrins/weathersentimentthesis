# Script to combined the weekly average weather values and weekly sentiment frequencies.
import sys, os, datetime, time

def main():
    # Get average sentiment per week
    sentiment = {}
    with open("data/classified.csv", "r", encoding="utf-8") as f:
        for line in f:
            try:
                parts = line.strip().split("\t")
                label = parts[2]
                date = time.strptime(parts[0][0:10], '%Y-%m-%d')
                weekNumber = datetime.date(date[0], date[1], date[2]).isocalendar()[1]
                if parts[0][0:4] == "2013":
                    if weekNumber not in sentiment:
                        sentiment[weekNumber] = {'p':0, 'n':0, 'o':0}
                    sentiment[weekNumber][label] += 1
            except Exception as e:
                print(e)
    # Get average weather variables per week per station
    weather = {}
    var = ['FG','TG','TN','TX','SQ','DR','RH']
    with open("data/KNMI_20131231_v1.txt", "r") as f:
        for line in f:
            line = line.strip()
            if len(line) == 0 or line[0] == "#":
                continue
            parts = line.split(",")
            station = parts[0]
            date = time.strptime(parts[1], '%Y%m%d')
            weekNumber = datetime.date(date[0], date[1], date[2]).isocalendar()[1]
            if station not in weather:
                weather[station] = {}
            if weekNumber not in weather[station]:
                weather[station][weekNumber] = {'FG':[],'TG':[],'TN':[],'TX':[],'SQ':[],'DR':[],'RH':[]}
            
            for i in range(len(var)):
                value = parts[i+2].strip()
                try:
                    value = int(value)
                except ValueError:
                    value = None
                if value is not None:
                    if value < 0:
                        value = 0
                    weather[station][weekNumber][var[i]].append(value)
    # Now we have a list of values per week per station
    # We want the average of these values per week
    weeklyWeather = {}
    # First combine all lists of each value of each station
    for station in weather:
        for weekNumber in weather[station]:
            if weekNumber not in weeklyWeather:
                weeklyWeather[weekNumber] = {'FG':[],'TG':[],'TN':[],'TX':[],'SQ':[],'DR':[],'RH':[]}
            for column in var:
                weeklyWeather[weekNumber][column].extend(weather[station][weekNumber][column])
    for weekNumber in weeklyWeather:
        for column in var:
            weeklyWeather[weekNumber][column] = sum(weeklyWeather[weekNumber][column]) / float(len(weeklyWeather[weekNumber][column]))
    # Print final data
    print("week\tp\tn\to\ttotal\tp\tn\to\t", end="")
    print("\t".join(var))
    for weekNumber in sorted(weeklyWeather.keys()):
        p = sentiment[weekNumber]["p"]
        n = sentiment[weekNumber]["n"]
        o = sentiment[weekNumber]["o"]
        total = p + n + o
        print("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t".format(weekNumber, p, n, o, total, p/total, n/total, o/total), end="")
        for column in var:
            print(weeklyWeather[weekNumber][column], end="\t")
        print("")

if __name__ == "__main__":
    main()
