# Used to count the sentiment and frequency of a given word per month.
import sys, io
import nltk.classify.util
from nltk.classify import NaiveBayesClassifier
from nltk.tokenize import word_tokenize
from urllib.parse import urlsplit

def getLabels():
    return ["p", "n", "o"]

def main():
    counter = {}
    counterMonthly = {}

    word = "storm"
    
    with open("data/classified.csv", "r", encoding="utf-8") as f:
        for line in f:
            try:
                date, tweet, label = line.strip().split("\t")
                yearMonth = date[0:7]
                if yearMonth not in counter:
                    counter[yearMonth] = {"p":0,"n":0,"o":0}
                if yearMonth not in counterMonthly:
                    counterMonthly[yearMonth] = 0
                counterMonthly[yearMonth] += 1
                if word in getTokens(tweet):
                    counter[yearMonth][label] += 1
            except:
                pass
    print("word: {}".format(word))
    print("month\tp\tn\to\ttotal\tmonthTotal\tmonthlyPerc")
    for yearMonth in sorted(counter.keys()):
        total = sum(counter[yearMonth].values())
        print("{}\t{}\t{}\t{}\t{}\t{}\t{}".format(yearMonth, total, counterMonthly[yearMonth], total/counterMonthly[yearMonth], counter[yearMonth]["p"]/total,
                                                  counter[yearMonth]["n"]/total, counter[yearMonth]["o"]/total))
    print("___")

def getTokens(body):
    return normalizeTokens(word_tokenize(body, language='dutch'))

def normalizeTokens(tokens):
    remove = []
    for i in range(len(tokens)):
        token = tokens[i].lower()
        if tokens[i] == "http" and tokens[i + 1] == ":":
            link = "http:"+tokens[i + 2]
            tokens[i] = urlsplit(link).netloc
            remove.append(i + 1)
            remove.append(i + 2)
    for index in reversed(remove):
        del tokens[index]
    return tokens

if __name__ == "__main__":
    main()
