package nl.hendrikprins.pt.misc;

import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import javafx.util.Pair;
import nl.hendrikprins.pt.gfx.ColorRange;
import nl.hendrikprins.pt.util.RDLatLng;

public class GridHeatmap {
	
	public GridHeatmap() throws IOException{
		String input = "E:\\Programming\\Projects\\WeatherSentiment\\data\\filtered_prepared.csv";
		String output = "E:\\Programming\\Projects\\WeatherSentiment\\data\\tweet-distribution.png";
		System.out.println("Loading data... ");
		List<Pair<Point, String>> data = new ArrayList<>();
		String line = null;
		BufferedReader br = new BufferedReader(new FileReader(new File(input)));
		Point min = new Point(999999,999999);
		Point max = new Point(0,0);
		while((line = br.readLine()) != null){
			String[] parts = line.split(";");
			String[] xy = parts[0].split(" ");
			Point point = RDLatLng.latLngToRD(Float.parseFloat(xy[1]), Float.parseFloat(xy[0]));
			if(point.y < 289000 || point.y > 629000 || point.x < -7 || point.x > 300000)continue;
			if(point.x < min.x) min.x = point.x;
			if(point.x > max.x) max.x = point.x;
			if(point.y < min.y) min.y = point.y;
			if(point.y > max.y) max.y = point.y;	
			data.add(new Pair<Point, String>(point, line));
		}
		br.close();
		System.out.println("Loaded data: "+data.size());
		System.out.println("Min: "+min.toString());
		System.out.println("Max: "+max.toString());
		
		System.out.println("Creating heatmap...");
		
		// Add margin
		min.x -= 10000;
		min.y -= 10000;
		max.x += 10000;
		max.y += 10000;
		
		int dataWidth = max.x - min.x;
		int dataHeight = max.y - min.y;
		int imgWidth = 500;
		int imgHeight = (int)((imgWidth/(double)dataWidth)*dataHeight);
		System.out.println("Image size: width="+imgWidth+", height="+imgHeight);
		
		// Count tweets per grid cell
		int gridSize = dataWidth / imgWidth;
		System.out.println(gridSize+"m per pixel");
		int[][] counter = new int[imgWidth+1][imgHeight+1];
		for(Pair<Point, String> item : data){
			int sx = (item.getKey().x - min.x) / gridSize;
			int sy = (item.getKey().y - min.y) / gridSize;
			if(sx >= 0 && sx < counter.length && sy >= 0 && sy < counter[sx].length){
				counter[sx][sy]++;
			}else{
				System.out.println("x: "+item.getKey().x+" sx:"+sx);
				System.out.println("y: "+item.getKey().y+" sy:"+sy);
			}
		}
		BufferedImage image = new BufferedImage(imgWidth+1, imgHeight+1, BufferedImage.TYPE_INT_ARGB);
		ColorRange.load();
		int maxCount = 0;
		int maxCount2 = 0;
		for(int x = 0;x < counter.length;x++){
			for(int y = 0;y < counter[x].length;y++){
				if(counter[x][y] > maxCount)maxCount = counter[x][y];
				if(counter[x][y] > maxCount2 && counter[x][y] < maxCount)maxCount2 = counter[x][y];
				image.setRGB(x, imgHeight-y, counter[x][y] <= 0 ? Color.black.getRGB() : ColorRange.blueToRedSimple.getByValue((float)counter[x][y], (float)20));
			}
		}
		ImageIO.write(image, "png", new File(output));
		System.out.println("Done");
	}
	
	
	
	public static void main(String[] args){
		try {
			new GridHeatmap();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
