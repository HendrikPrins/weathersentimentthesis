# Used to split the dataset into three parts: training (manual), training (bootstrap) and testing (manual)
import sys, os, random

def main():
    data = []
    with open("data/filtered_prepared.csv", "r", encoding="utf-8") as streamIn:
        for line in streamIn:
            data.append(line)
    random.shuffle(data)
    writeToFile(data[0:2000], "data/train_manual.csv")
    writeToFile(data[2000:4000], "data/test_manual.csv")
    writeToFile(data[4000:6000], "data/extra_manual.csv")
    writeToFile(data[6000:], "data/train_bootstrap.csv")

def writeToFile(data, file):
    with open(file, "w", encoding="utf-8") as f:
        for entry in data:
            f.write(entry)
if __name__ == "__main__":
    main()