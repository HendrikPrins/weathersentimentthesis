# 
import sys, io, heapq, csv
import nltk.classify.util
from nltk.classify import NaiveBayesClassifier
from nltk.tokenize import word_tokenize
from urllib.parse import urlsplit

def getLabels():
    return ["p", "n", "o"]

def main():

    monthlyCounter = {}
    monthlyUserCounter = {}
    
    with open("data/filtered_prepared.csv", "r", encoding="utf-8") as f:
        reader = csv.reader(f, delimiter=";")
        for line in reader:
            tweet = line
            user = tweet[3]
            yearMonth = tweet[4][0:7]
            if yearMonth[0] != "2":
                print("{}\n{} {}\n\n".format(line, user, yearMonth))
            if yearMonth not in monthlyCounter:
                monthlyCounter[yearMonth] = 0
            if yearMonth not in monthlyUserCounter:
                monthlyUserCounter[yearMonth] = set()
            monthlyCounter[yearMonth] += 1
            monthlyUserCounter[yearMonth].add(user)
    print("month\ttweets\tusers")
    for yearMonth in sorted(monthlyCounter.keys()):
        print("{}\t{}\t{}".format(yearMonth, monthlyCounter[yearMonth], len(monthlyUserCounter[yearMonth])))
    print("___")

# Returns the split and cleaned tweet based on the given line
def parseTweet(tweet):
    parts = tweet.strip().split(";", 8)
    if parts[8][0] == "\"":
        parts[8] = parts[8][1:]
    if parts[8][-1] == "\"":
        parts[8] = parts[8][:-1]
    return parts

if __name__ == "__main__":
    main()
