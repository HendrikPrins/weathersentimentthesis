# Used to count the sentiment per month
import sys, io, datetime, time

def getLabels():
    return ["p", "n", "o"]

def main():
    counter = {}
    
    with open("data/classified.csv", "r", encoding="utf-8") as f:
        for line in f:
            try:
                date, tweet, label = line.strip().split("\t")
                date = time.strptime(date[0:10], '%Y-%m-%d')
                weekNumber = datetime.date(date[0], date[1], date[2]).isocalendar()[1]
                key = int(date[0])*1000 + weekNumber
                if key not in counter:
                    counter[key] = {"week":weekNumber,"year":date[0],"p":0,"n":0,"o":0}
                counter[key][label] += 1
            except Exception as e:
                pass
    sep = "\t"
    print("sorting"+sep+"week_year"+sep+"positive"+sep+"negative"+sep+"objective")
    for key in sorted(counter.keys()):
        total = counter[key]["p"] + counter[key]["n"] + counter[key]["o"]
        print(("{}"+sep+"{} ({})"+sep+"{}"+sep+"{}"+sep+"{}").format(key, counter[key]["week"], counter[key]["year"], counter[key]["p"]/total, counter[key]["n"]/total, counter[key]["o"]/total))
    print("___")


if __name__ == "__main__":
    main()
